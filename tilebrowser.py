import json

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("WebKit2", "4.0")
from gi.repository import Gtk, WebKit2, GLib

webtiles = []
containers = []


class Container:
    def __init__(self, settings):
        orientation = getattr(Gtk.Orientation, settings["orientation"].upper())
        self.pane = Gtk.Paned.new(orientation)
        self.position = settings["dividerPosition"]

    def resize(self):
        self.pane.set_position(
            self.pane.props.max_position * (self.position / 100)
        )


class WebTile:
    def __init__(self, url, reload_interval=0):
        self._url = url
        self._count = 1
        self._reload_interval = reload_interval
        self.webview = WebKit2.WebView()
        self.webview.load_uri(self._url)

    def reload(self):
        if self._reload_interval > 0:
            if self._count == self._reload_interval:
                self.webview.load_uri(self._url)
                self._count = 1
            else:
                self._count += 1


class WebTileReloader:
    def __init__(self, webtiles):
        self._webtiles = webtiles

    def reload(self):
        for webtile in self._webtiles:
            webtile.reload()
        return True

    def start(self):
        GLib.timeout_add(1000, self.reload)


def generate_element(element):
    if isinstance(element, list):
        settings, one, two = element
        container = Container(settings)
        container.pane.add(generate_element(one))
        container.pane.add(generate_element(two))
        containers.append(container)
        return container.pane
    elif isinstance(element, dict):
        webtile = WebTile(**element)
        webtiles.append(webtile)
        return webtile.webview
    else:
        raise Exception


def load_layout():
    with open("layout.json") as f:
        return json.load(f)


def check_resize(window):
    for container in containers:
        container.resize()


if __name__ == '__main__':
    window = Gtk.Window()
    window.connect("destroy", Gtk.main_quit)
    window.connect("check-resize", check_resize)
    window.set_title("Dashboard")

    vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)

    button = Gtk.Button.new_with_label("Start reloading")

    vbox.pack_start(button, False, True, 0)
    vbox.pack_start(generate_element(load_layout()), True, True, 0)

    reloader = WebTileReloader(webtiles)

    def reload(button):
        reloader.start()
        button.destroy()
        window.fullscreen()
    button.connect("clicked", reload)

    window.add(vbox)
    window.maximize()
    window.show_all()

    Gtk.main()
